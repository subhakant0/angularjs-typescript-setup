import { NgModule } from 'angular-ts-decorators';
import { AppComponent } from './app.component';
import './styles.css';

@NgModule({
    id: 'AppModule',
    imports: [],
    declarations: [AppComponent],
    providers: [],
    bootstrap: [AppComponent]
})
export class AppModule {}
